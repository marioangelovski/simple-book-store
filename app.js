$(function () {
    const app = new App();
	function App(){
        this.id = 0;
        this.Books = [];

       this.addTrueNatureList = function(Book){
        this.Books.push(Book);
       }

       this.printList = function(){
            let htmlToAdd = "";
                
            for(let index=0; index <= this.Books.length-1; index++) {
                let book = this.Books[index];
                htmlToAdd += `<li class="card" data-book-index="${index}">
                    <div class="card-title"> ${book.title} </div>
                    <div class="card-img"> <img src="https://bit.ly/2PxhFm8"> </div>
                    <div class="card-description"> ${book.description} </div>
                    <div class="card-price"> ${book.price} </div>
                    <div class="card-quantity">${book.quantity}</div>
                    <div class="card-actions"> 
                    <button class="buy-btn btn buy"> buy </button> 
                    <button class="wishlist-btn btn"> add to wishlist </button>
                        </li>`;
            }
                    
            $("#books-list").html(htmlToAdd);

            $(".buy-btn").on('click', function() {
                let htmlToAdd = "";

                let selected = this.closest("li");
                let selectedBook = app.Books[selected.dataset.bookIndex];
                selectedBook.quantity--;
                let q = $(selected).find('.card-quantity')[0];
                $(q).html(selectedBook.quantity);

                if (selectedBook.quantity == 0) {
                    console.log('disebjliraj');
                    let bb = $(selected).find('.buy-btn')[0];
                    console.log(bb);
                    $(bb).attr('disabled', true);
                }

                htmlToAdd += `<li> ${selectedBook.title} <a href="#" class="remove-book-card" id="1"> remove </a> </li>`
                $("#shopping-card").append(htmlToAdd);

                app.deleteEvent();
               
            });

            $(".wishlist-btn").on("click", function(){
                let htmlToAdd = "";
                let selected = this.closest("li");
                let selectedBook = app.Books[selected.dataset.bookIndex];
                
                htmlToAdd += `<li>${selectedBook.title} <a href="#" class="remove-book-wish" id="1"> remove </a></li>`
                $("#wishlist").append(htmlToAdd);

                app.deleteEvent();
            });
            
            this.deleteEvent = function() {

                $(".remove-book-card").on("click", function() {
                    this.closest('li').remove();
                });
                $(".remove-book-wish").on("click", function() {
                    this.closest('li').remove();
                });
            }



        }



    }
    


    function Book(title, author, cover, price, quantity, description) {
        this.id = app.id++;
        this.title = title.value;
        this.author = author.value;
        this.cover = cover.value;
        this.price = price.value;
        this.quantity = quantity.value;
        this.description = description.value;
    }
    $("#add-btn").on("click", function(e){
        e.preventDefault();
        console.log("add btn was clicked");
        
        app.addTrueNatureList(new Book(title, author, cover, price, quantity, description));
        app.printList();
    })
});